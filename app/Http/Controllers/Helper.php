<?php

namespace App\Http\Controllers;

use stdClass;
use Carbon\Carbon;
use Auth;
use Session;

use App\Notifications\FirebasePushNotif;





use App\User;


trait Helper
{
    protected function getBaseUrl($addLastSlash = true, $suffix = null)
    {
        if($addLastSlash)
            $removeString = 'base-url';
        else
            $removeString = '/base-url';
        $base = str_replace($removeString, '', route('base_url'));

        if($addLastSlash && $suffix && $suffix[0] != '/')
            $suffix = '/'.$suffix;

        return $base.$suffix;
    }

    public function flatten(array $array)
    {
        $return = array();
        array_walk_recursive($array, function($a,$b) use (&$return){
            $return[$b] = $a;
        });
        return $return;
    }
    
    /**
     * Send (push) a Notification to a user.
     *
     * @param   eloquent    $user
     * @param   string      $title
     * @param   string      $body
     * @param   string      $label      (must be in kebab-case)
     * @param   string      $id         nullable
     * @param   timestamp   $timestamp  nullable
     * @return  void
     */
    public function pushNotif($user, $title, $body, $label, $id = null, $timestamp = null)
    {
        if(!$id && !$timestamp)
            $notification = new FirebasePushNotif($title, $body, $label);
        else
        {
            $detail = new StdClass;
            $detail->id = $id;
            $detail->timestamp = $timestamp;

            $notification = new FirebasePushNotif($title, $body, $label, $detail);
            
            // if($user && $user->nip)
            // {
            //     $modulNotif=new NotifHelperController;
            //     $modulNotif->addTableNotif($id, $label, $user->nip, $body);
            // }
        }

        if($user)
        {
            $notify = $user->notify($notification);
        }
        
        // Log::create([
        //     'jenis' => $label,
        //     'isi'   => 'push notif '.$label.' in helper',
        // ]);


        return $notification;
    }

    protected function cleanPath($path)
    {
        $path = str_replace('\\', '/', $path);
        if (DIRECTORY_SEPARATOR == '\\')    // windows
            $path = str_replace('/', '\\', $path);

        return $path;
    }
}
